package com.epam.CollectionsHomeTask1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Question7 {
	
	public static  Map<Employee,Integer>modify(final Map<Employee,Integer> map)
	{
		Comparator<Employee> comp=new Comparator<Employee>() {

			@Override
			public int compare(Employee emp1, Employee emp2) {
				return emp2.getName().compareTo(emp1.getName());
			}
		};
		TreeMap<Employee,Integer> newMap=new TreeMap<Employee,Integer>(comp);
		newMap.putAll(map);
		return newMap;
	}

	public static void main(String[] args) {
		TreeMap<Employee,Integer> treeMap=new TreeMap<Employee,Integer>();
		treeMap.put(new Employee("Rohit",101,200000),1);
		treeMap.put(new Employee("Priyanka",102,175000),2);
		treeMap.put(new Employee("Bhargav",105,150000),3);
		treeMap.put(new Employee("Manoj",107,50000),4);
		treeMap.put(new Employee("Poojitha",103,50000),5);
		treeMap.put(new Employee("Komali",109,100000),6);
		System.out.println("Original tree map :");
//		List<Integer> l=new ArrayList<>(treeMap.values());
//		System.out.println(l);
		for(Map.Entry<Employee, Integer> entry:treeMap.entrySet()) {
			System.out.println("key :");
			System.out.println(entry.getKey());
			System.out.println("value :");
			System.out.println(entry.getValue());
			System.out.println();
		}
		TreeMap<Employee,Integer> sortedTreeMap=(TreeMap<Employee, Integer>) modify(treeMap);
		System.out.println("Employees in the descending order of their names :");
		for(Map.Entry<Employee, Integer> entry:sortedTreeMap.entrySet()) {
			System.out.println("key :");
			System.out.println(entry.getKey());
			System.out.println("value :");
			System.out.println(entry.getValue());
			System.out.println();
		}
		
	}

}
