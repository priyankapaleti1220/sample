package com.epam.CollectionsHomeTask1;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class Question6{
	public static  Map<String,Integer> conclude(final Map<String,Integer> map)
	{
		Comparator<String> comp=new Comparator<String>() {

			@Override
			public int compare(String key1, String key2) {
				return map.get(key2).compareTo(map.get(key1));
			}
		};
		TreeMap<String,Integer> newMap=new TreeMap<String,Integer>(comp);
		newMap.putAll(map);
		return newMap;
	}

	public static void main(String[] args) {
		TreeMap<String,Integer> treeMap=new TreeMap<String,Integer>();
		treeMap.put("Bhanu",1);
		treeMap.put("Manoj",-5);
		treeMap.put("Rohit",10);
		treeMap.put("Priyanka",11);
		treeMap.put("Bhargav",5);
		TreeMap<String,Integer> sortedTreeMap=(TreeMap<String, Integer>) conclude(treeMap);
		System.out.println("Original tree map :");
		System.out.println(treeMap);
		System.out.println();
		System.out.println("Tree map after sorting based on values in descending order :");
		System.out.println(sortedTreeMap);
		
		

	}

}
