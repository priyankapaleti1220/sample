package com.epam.CollectionsHomeTask1;
import java.util.*;

public class Question1 {

	public static void main(String[] args) {
		
	List<Integer> numberList=new ArrayList<Integer>();
	numberList.add(100);
	numberList.add(100);
	numberList.add(-5);
	numberList.add(100);
	numberList.add(1);
	numberList.add(7);
	System.out.println("Given list of numbers :");
	System.out.println(numberList);
	System.out.println();
	Set<Integer> sorted=new HashSet<Integer>(numberList);
	List<Integer> sortedList=new ArrayList<Integer>(sorted);
	Collections.sort(sortedList,Collections.reverseOrder());
	System.out.println("Second biggest number in the given list of numbers :");
	System.out.println(sortedList.get(1));

	}

}
