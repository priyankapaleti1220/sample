package com.epam.CollectionsHomeTask1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class Question2 {

	public static void main(String[] args) {
		List<Integer> numberList=new ArrayList<Integer>();
		numberList.add(2);
		numberList.add(20);
		numberList.add(99);
		numberList.add(6);
		numberList.add(-125);
		numberList.add(-200);
		numberList.add(500);
		numberList.add(-3);
		System.out.println("Original List :");
		System.out.println(numberList);
		System.out.println();
		Collections.sort(numberList,new Comparator<Integer>()
		 {
	       @Override
	       public int compare(Integer num1,Integer num2)
	       {
	    	   return num2-num1;
	       }
		 });
		System.out.println("Original list sorted in reverse order :");
		System.out.println(numberList);

	}

}
