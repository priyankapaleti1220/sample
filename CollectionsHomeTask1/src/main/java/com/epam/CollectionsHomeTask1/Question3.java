package com.epam.CollectionsHomeTask1;
import java.util.*;

public class Question3 {

	public static void main(String[] args) {
		List<Employee> employeeList =new ArrayList<Employee>();
		employeeList.add(new Employee("Rohit",101,200000));
		employeeList.add(new Employee("Priyanka",102,175000));
		employeeList.add(new Employee("Bhargav",105,150000));
		employeeList.add(new Employee("Manoj",107,50000));
		employeeList.add(new Employee("Poojitha",103,50000));
		employeeList.add(new Employee("Komali",109,100000));
		System.out.println("Original employee list :");
		System.out.println(employeeList);
		System.out.println();
		Collections.sort(employeeList,new Comparator<Employee>() {
			public int compare(Employee emp1,Employee emp2)
			{
				return emp1.getName().compareTo(emp2.getName());
			}
		});
		System.out.println("Employee list after sording them based on their names :");
		System.out.println(employeeList);
		
	}

}
