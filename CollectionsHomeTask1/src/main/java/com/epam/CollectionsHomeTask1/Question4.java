package com.epam.CollectionsHomeTask1;

import java.util.Set;
import java.util.TreeSet;

public class Question4 {
	public static void main(String args[])
	{
		TreeSet<Integer> numberList=new TreeSet<Integer>();
		numberList.add(500);
		numberList.add(-25);
		numberList.add(7);
		numberList.add(-10);
		numberList.add(32);
		numberList.add(46);
		numberList.add(3);
		System.out.println("Original treeset :");
		System.out.println(numberList);
		System.out.println();
		Set<Integer> reverseSorted=numberList.descendingSet();
		System.out.println("Treeset after sorting in descending order :");
		System.out.println(reverseSorted);
		
	}

}
