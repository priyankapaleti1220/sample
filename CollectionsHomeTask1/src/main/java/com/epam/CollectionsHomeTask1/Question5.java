package com.epam.CollectionsHomeTask1;
import java.util.TreeSet;

public class Question5 {

	public static void main(String[] args) {
		TreeSet<Employee> employeeList =new TreeSet<Employee>();
		employeeList.add(new Employee("Rohit",101,200000));
		employeeList.add(new Employee("Priyanka",102,175000));
		employeeList.add(new Employee("Bhargav",105,150000));
		employeeList.add(new Employee("Manoj",107,50000));
		employeeList.add(new Employee("Poojitha",103,50000));
		employeeList.add(new Employee("Komali",109,100000));
		System.out.println("Tree set in alphabetical order of their names :");
		System.out.println(employeeList);

	}

}
