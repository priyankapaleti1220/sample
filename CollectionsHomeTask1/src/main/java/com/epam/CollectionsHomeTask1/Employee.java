package com.epam.CollectionsHomeTask1;

public class Employee implements Comparable<Employee>{
	private String name;
	private int id;
	private double salary;
	Employee(String name,int id,double salary)
	{
		this.name=name;
		this.id=id;
		this.salary=salary;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setId(int id)
	{
		this.id=id;
	}
	public void setSalary(double salary)
	{
		this.salary=salary;
	}
	public String getName()
	{
		return name;
	}
	public int getId()
	{
		return id;
	}
	public double getSalary()
	{
		return salary;
	}
	public String toString()
	{
		return "name :"+name+" id :"+id+" salary :"+salary;
	}
	@Override
	public int compareTo(Employee e)
	{
		return this.name.compareTo(e.name);
	}
	
}
